const getAllReviewMiddleware = (request, response, next) => {
    console.log("Get ALL Review Middleware");
    next();
}

const createReviewMiddleware = (request, response, next) => {
    console.log("Create Review Middleware");
    next();
}

const getDetailReviewMiddleware = (request, response, next) => {
    console.log("Get Detail Review Middleware");
    next();
}

const updateReviewMiddleware = (request, response, next) => {
    console.log("Update Review Middleware");
    next();
}

const deleteReviewMiddleware = (request, response, next) => {
    console.log("Delete Review Middleware");
    next();
}

module.exports = {
    getAllReviewMiddleware,
    createReviewMiddleware,
    getDetailReviewMiddleware,
    updateReviewMiddleware,
    deleteReviewMiddleware
}